var express = require('express');
var router = express.Router();
var jwt = require('express-jwt');
var auth = jwt({
  secret: process.env.JWT_SECRET,
  userProperty: 'payload',
});
var ctrlAuth = require('../controllers/authentication');
var ctrlNotes = require('../controllers/notes');

// authentication
router.post('/register', ctrlAuth.register);
router.post('/login', ctrlAuth.login);

// notes
router.post('/notes', auth, ctrlNotes.notesCreate);
router.get('/notes', auth, ctrlNotes.notesReadAll);
router.get('/notes/:noteId', auth, ctrlNotes.notesReadOne);
router.put('/notes/:noteId', auth, ctrlNotes.notesUpdateOne);
router.delete('/notes/:noteId', auth, ctrlNotes.notesDeleteOne);

module.exports = router;
