var mongoose = require('mongoose');
var User = mongoose.model('User');

var sendJsonResponse = function (res, status, content) {
  res.status(status);
  res.json(content);
}

module.exports.notesCreate = function (req, res) {
  if (!req.payload || !req.payload._id) {
    sendJsonResponse(res, 404, {
      "message": "Missed user _id in the jwt payload"
    });
    return;
  }
  User
    .findById(req.payload._id)
    .select('notes')
    .exec(function(err, user) {
      if (!user) {
        sendJsonResponse(res, 404, {
          "message": "User not found"
        });
      }
      else if (err) {
        sendJsonResponse(res, 400, err);
      } else {
        user.notes.push({
          name: req.body.name,
          noteText: req.body.noteText,
        });
        user.save(function(err, user) {
          var thisNote;
          if (err) {
            sendJsonResponse(res, 400, err);
          } else {
            thisNote = user.notes[user.notes.length - 1];
            sendJsonResponse(res, 201, thisNote);
          }
        });
      }
    });
};

module.exports.notesReadOne = function(req, res) {
  if (!req.payload || !req.payload._id) {
    sendJsonResponse(res, 404, {
      "message": "Missed user _id in the jwt payload"
    });
    return;
  }
  User
    .findById(req.payload._id)
    .select('notes')
    .exec(function(err, user) {
      var note;
      if (!user) {
        sendJsonResponse(res, 404, {
          "message": "User not found"
        });
      } else if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        note = user.notes.id(req.params.noteId);
        if (!note) {
          sendJsonResponse(res, 404, {
            "message": "note not found"
          });
        } else {
          sendJsonResponse(res, 200, note);
        }
      }
    });
};

module.exports.notesReadAll = function(req, res) {
  if (!req.payload || !req.payload._id) {
    sendJsonResponse(res, 404, {
      "message": "Missed user _id in the jwt payload"
    });
    return;
  }
  User
    .findById(req.payload._id)
    .select('notes')
    .exec(function(err, user) {
      if (!user) {
        sendJsonResponse(res, 404, {
          "message": "User not found"
        });
      } else if (err) {
        sendJsonResponse(res, 404, err);
      } else {
        sendJsonResponse(res, 200, user.notes);
      }
    });
};

module.exports.notesUpdateOne = function (req, res) {
  if (!req.payload || !req.payload._id) {
    sendJsonResponse(res, 404, {
      "message": "Missed user _id in the jwt payload"
    });
    return;
  }
  User
    .findById(req.payload._id)
    .select('notes')
    .exec(function(err, user) {
      var note;
      if (!user) {
        sendJsonResponse(res, 404, {
          "message": "User not found"
        });
      } else if (err) {
        sendJsonResponse(res, 400, err);
      } else {
        note = user.notes.id(req.params.noteId);
        if (!note) {
          sendJsonResponse(res, 404, {
            "message": "Note not found"
          });
        } else {
          note.name = req.body.name;
          note.noteText = req.body.noteText;
          user.save(function(err, user) {
            if (err) {
              sendJsonResponse(res, 404, err);
            } else {
              sendJsonResponse(res, 200, note);
            }
          });
        }
      }
    });
};

module.exports.notesDeleteOne = function (req, res) {
  if (!req.payload || !req.payload._id) {
    sendJsonResponse(res, 404, {
      "message": "Missed user _id in the jwt payload"
    });
    return;
  }
  User
    .findById(req.payload._id)
    .select('notes')
    .exec(function(err, user) {
      var note;
      if (err) {
        sendJsonResponse(res, 404, err);
      } else if (!user) {
        sendJsonResponse(res, 404, {
          "message": "User not found"
        });
      } else {
        note = user.notes.id(req.params.noteId);
        if (!note) {
          sendJsonResponse(res, 404, {
            "message": "Note not found"
          });
        } else {
          note.remove();
          user.save(function(err) {
            if (err) {
              sendJsonResponse(res, 404, err);
            } else {
              sendJsonResponse(res, 204, null);
            }
          });
        }
      }
    });
};
