var mongoose = require('mongoose');
var gracefulShutdown;
var dbURI = process.env.MONGOSERVER_URI;

// fixing deprecation warnings
mongoose.set('useNewUrlParser', true);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);

mongoose.connect(dbURI).catch(function(error) {
  console.log('Mongoose connection error: ' + error);
});

mongoose.connection.on('connected', function() {
  console.log('Mongoose connected to ' + dbURI);
});
mongoose.connection.on('error', function(error) {
  console.log('Mongoose connection error: ' + error);
});
mongoose.connection.on('disconnected', function() {
  console.log('Mongoose disconnected');
});
gracefulShutdown = function (message, callback) {
  mongoose.connection.close(function () {
    console.log('Mongoose disconnected through ' + message);
    callback();
  });
};
// For nodemon restarts
process.once('SIGUSR2', function() {
  gracefulShutdown('nodemon restarts', function() {
    process.kill(process.pid, 'SIGUSR2');
  })
});
// For app termination
process.on('SIGINT', function() {
  gracefulShutdown('app termination', function() {
    process.exit(0);
  })
});
// For Heroku app termination
process.on('SIGTERM', function() {
  gracefulShutdown('Heroku app termination', function() {
    process.exit(0);
  })
});

require('./users');
