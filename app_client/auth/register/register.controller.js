(function() {

  angular
    .module('notesk')
    .controller('register', register);

  register.$inject = ['$location', 'authentication'];
  function register($location, authentication) {
    var vm = this;
    vm.pageHeader = {
      title: 'Create a new Notesk account',
    };
    vm.credentials = {
      name: "",
      email: "",
      password: "",
    };
    vm.onSubmit = function() {
      vm.formError = "";
      if (!vm.credentials.name || !vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        vm.doRegister();
      }
    };
    vm.doRegister = function() {
      vm.formError = "";
      authentication
        .register(vm.credentials)
        .then(function() {
          $location.path('/')
        })
        .catch(function(response) {
          vm.formError = response.data;
        });
    };
  }

})();
