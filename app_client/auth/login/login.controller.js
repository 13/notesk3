(function() {

  angular
    .module('notesk')
    .controller('login', login);

  login.$inject = ['$location', 'authentication'];
  function login($location, authentication) {
    var vm = this;
    vm.pageHeader = {
      title: 'Sign in to Notesk',
    };
    vm.credentials = {
      email: "",
      password: "",
    };
    vm.onSubmit = function() {
      vm.formError = "";
      if (!vm.credentials.email || !vm.credentials.password) {
        vm.formError = "All fields required, please try again";
        return false;
      } else {
        vm.doLogin();
      }
    };
    vm.doLogin = function() {
      vm.formError = "";
      authentication
        .login(vm.credentials)
        .then(function() {
          $location.path('/')
        })
        .catch(function(response) {
          vm.formError = response.data;
        });
    };
  }

})();
