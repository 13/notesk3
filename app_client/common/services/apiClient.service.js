(function() {

  angular
    .module('notesk')
    .service('apiClient', apiClient);

  apiClient.$inject = ['$http', 'authentication'];
  function apiClient($http, authentication) {
    var getConfig = function() {
      return {
        headers: {
          Authorization: 'Bearer ' + authentication.getToken(),
        },
      };
    };
    this.createNote = function(note) {
      return $http.post('/api/notes', note, getConfig());
    };
    this.getNotesList = function() {
      return $http.get('/api/notes', getConfig());
    };
    this.updateNote = function(note) {
      return $http.put('/api/notes/' + note._id, note, getConfig());
    };
    this.deleteNote = function(note) {
      return $http.delete('/api/notes/' + note._id, getConfig());
    };
  };

})();
