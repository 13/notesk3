(function() {

  angular
    .module('notesk')
    .controller('navigation', navigation);

  navigation.$inject = ['$location', 'authentication'];
  function navigation($location, authentication) {
    var vm = this;
    vm.currentPath = $location.path();
    vm.isLoggedIn = authentication.isLoggedIn();
    vm.currentUser = authentication.currentUser();
    vm.logout = function() {
      authentication.logout();
      vm.isLoggedIn = false;
      $location.path('/login');
    };
  }

})();
