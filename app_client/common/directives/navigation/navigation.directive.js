(function() {

  angular
    .module('notesk')
    .directive('navigation', navigation);

  function navigation() {
    return {
      restrict: 'EA',
      templateUrl: '/common/directives/navigation/navigation.template.html',
      controller: 'navigation as navvm',
    };
  }

})();
