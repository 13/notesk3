(function() {

  angular
    .module('notesk')
    .factory('Note', ['apiClient', noteFactory]);

  function noteFactory(apiClient) {
    function Note(noteData) {
      this.editing = false;
      this.pending = {
        save: false,
        delete: false,
      };
      this.saved = Boolean(noteData);
      this.timeout = false;
      this.errorMessage = '';
      this.data = noteData || {};
      this.delayBeforeSave = 2000;
    }
    Note.prototype = {
      existsOnServer: function() {
        return Boolean(this.data._id);
      },
      isPending: function() {
        return this.pending.save || this.pending.delete;
      },
      save: function() {
        var actionName = this.existsOnServer()? 'updateNote' : 'createNote';
        this.editing = false;
        this.pending.save = true;
        apiClient[actionName](this.data)
          .then(function(response) {
            this.updateAfterSave(response.data);
            if (!this.editing) {
              this.saved = true;
            }
          }.bind(this))
          .catch(function(response) {
            this.errorMessage = response.data.name + ': ' + response.data.message;
          }.bind(this))
          .finally(function() {
            this.pending.save = false;
            if (this.editing) {
              this.startEditing();
            }
          }.bind(this));
      },
      updateAfterSave: function(savedNoteData) {
        if (!this.data._id) {
          // setting _id if the saved item was a brand new note
          this.data._id = savedNoteData._id;
        }
        this.data.updatedAt = savedNoteData.updatedAt;
      },
      startEditing: function() {
        this.errorMessage = '';
        this.editing = true;
        this.saved = false;
        if (this.timeout) {
          clearTimeout(this.timeout);
        }
        this.timeout = setTimeout(function() {
          if (!this.pending.save) {
            this.save();
          }
        }.bind(this), this.delayBeforeSave);
      },
      delete: function() {
        this.pending.delete = true;
        return apiClient
          .deleteNote(this.data)
          .catch(function(response) {
            this.errorMessage = response.data.name + ': ' + response.data.message;
          }.bind(this))
          .finally(function() {
            this.pending.delete = false;
          }.bind(this));
      },
    };
    return Note;
  };

})();
