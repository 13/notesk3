(function() {

  angular.module('notesk', ['ngRoute']);

  function config($routeProvider, $locationProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'home/home.view.html',
        controller: 'home',
        controllerAs: 'vm',
      })
      .when('/register', {
        templateUrl: 'auth/register/register.view.html',
        controller: 'register',
        controllerAs: 'vm',
      })
      .when('/login', {
        templateUrl: 'auth/login/login.view.html',
        controller: 'login',
        controllerAs: 'vm',
      })
      .otherwise({
        redirectTo: '/'
      });
    $locationProvider.html5Mode({
      enabled: true,
      requireBase: false,
    });
  }

  angular
    .module('notesk')
    .config(['$routeProvider', '$locationProvider', config]);

})();
