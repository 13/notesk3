(function() {

  angular
    .module('notesk')
    .controller('home', ['$location', '$window', 'apiClient', 'authentication', 'Note', home]);

  function home($location, $window, apiClient, authentication, Note) {
    var vm = this;
    if (!authentication.isLoggedIn()) {
      $location.path('/login');
      return;
    }
    vm.errorMessage = '';
    vm.notesListPending = true;
    vm.notesList = [];
    vm.currentNote = null;
    vm.setCurrentNote = function(note) {
      vm.currentNote = note;
    };
    vm.createNote = function() {
      var note = new Note();
      vm.notesList.push(note);
      vm.setCurrentNote(note);
    };
    vm.removeFromList = function(note) {
      if (note === vm.currentNote) {
        vm.setCurrentNote(null);
      }
      vm.notesList.splice(vm.notesList.indexOf(note), 1);
    };
    vm.deleteNote = function(note) {
      var confirmed = $window.confirm('Are you sure? This action cannot be undone.');
      if (confirmed) {
        if (note.existsOnServer()) {
          note
            .delete()
            .then(function() {
              vm.removeFromList(note);
            });
        } else {
          vm.removeFromList(note);
        }
      }
    };
    $window.onbeforeunload = function() {
      for (var i = 0; i < vm.notesList.length; i++) {
        if (!vm.notesList[i].saved) {
          return 'You may lose the unsaved data if you leave. Are you sure you want to continue?';
        }
      };
    };
    apiClient
      .getNotesList()
      .then(function(response) {
        for (var i = 0; i < response.data.length; i++) {
          vm.notesList.push(new Note(response.data[i]));
        }
      })
      .catch(function(response) {
        vm.errorMessage = response.data.message;
      })
      .finally(function() {
        vm.notesListPending = false;
      });
  }

})();
