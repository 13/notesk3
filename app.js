require('dotenv').config();
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var uglifyJs = require('uglify-js');
var fs = require('fs');
var passport = require('passport');

require('./app_api/models/db');
require('./app_api/config/passport');

var apiRouter = require('./app_api/routes/index');

var app = express();

var appClientFiles = [
  fs.readFileSync('app_client/app.js', 'utf8'),
  fs.readFileSync('app_client/home/home.controller.js', 'utf8'),
  fs.readFileSync('app_client/common/services/apiClient.service.js', 'utf8'),
  fs.readFileSync('app_client/common/services/authentication.service.js', 'utf8'),
  fs.readFileSync('app_client/common/directives/navigation/navigation.controller.js', 'utf8'),
  fs.readFileSync('app_client/common/directives/navigation/navigation.directive.js', 'utf8'),
  fs.readFileSync('app_client/auth/register/register.controller.js', 'utf8'),
  fs.readFileSync('app_client/auth/login/login.controller.js', 'utf8'),
  fs.readFileSync('app_client/note/note.factory.js', 'utf8'),
];
var uglified = uglifyJs.minify(appClientFiles, {
  compress: false
});

fs.writeFile('public/javascripts/app.min.js', uglified.code, function(err) {
  if (err) {
    console.log(err);
  } else {
    console.log('Script generated and saved: public/javascripts/app.min.js');
  }
});

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());

app.use('/api', apiRouter);

app.use(function(req, res) {
  res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

module.exports = app;
